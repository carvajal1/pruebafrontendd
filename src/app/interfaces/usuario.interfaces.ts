export interface Usuario{

  status: string,
  error: string,
  usuario:
    {
      id: number,
      nombre: string,
      apellido: string,
      tipoIdentificacion: string,
      numeroIdentificacion: string,
      email: string
    },

  detalle: string

}
