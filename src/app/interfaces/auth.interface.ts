export interface Auth{

    accessToken: string,
    encryptedAccessToken: string,
    expireInSeconds: number,
    userId: number,
    error: string

}
