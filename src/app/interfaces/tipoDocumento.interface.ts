export interface TipoDocumento{

  status: string,
  error: string,
  tipo: [
    {
      codigo: string,
      detalle: string
    }
  ],
  detalle: string

}
