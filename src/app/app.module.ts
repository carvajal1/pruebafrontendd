import { ServiceUsuarioService } from './services/service-usuario.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { app_routing } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login/login.component';
import { RegistraUsuarioComponent } from './admin/registrar/registra-usuario/registra-usuario.component';
import { ActualizaUsuarioComponent } from './admin/actualizar/actualiza-usuario/actualiza-usuario.component';
import { BuscarUsuarioComponent } from './admin/buscar/buscar-usuario/buscar-usuario.component';
import { EliminarUsuarioComponent } from './admin/eliminar/eliminar-usuario/eliminar-usuario.component';
import { HttpClientModule } from '@angular/common/http';
import { UsuarioComponent } from './admin/usuario/usuario.component';
import { JuegoComponent } from './admin/juego/juego.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistraUsuarioComponent,
    ActualizaUsuarioComponent,
    BuscarUsuarioComponent,
    EliminarUsuarioComponent,
    UsuarioComponent,
    JuegoComponent
  ],
  imports: [
    BrowserModule,
    app_routing,
    FormsModule,
    MatProgressSpinnerModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [ServiceUsuarioService,CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
