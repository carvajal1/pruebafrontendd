import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ServiceUsuarioService } from '../../services/service-usuario.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: string;
  password: string;
  show = false;

  constructor(private router: Router,private service: ServiceUsuarioService) { }

  ngOnInit(): void {
  }
  login(): void{
    this.show = true;
    const fd = new FormData();
    fd.append('identificacion', this.usuario);
    fd.append('Password', this.password);
    this.service.iniciaSession(fd).subscribe(
      resultado => {
        if (this.service.getToken() != "null"){
          alert("Usuario logeado");
          console.log(this.service.getToken());
          this.show=false;
          this.router.navigateByUrl('usuario');
        }else{
          console.log("error"+this.service.getToken());
          alert(this.service.getError());
          this.show=false;
        }
      }
    );
  }

  registrar(): void{
    this.router.navigateByUrl('registra');
  }

}
