import { Component, Input, OnInit } from '@angular/core';
import { DataAccessServiceService } from 'src/app/services/data-access-service.service';
import { ServiceUsuarioService } from 'src/app/services/service-usuario.service';

@Component({
  selector: 'app-buscar-usuario',
  templateUrl: './buscar-usuario.component.html',
  styleUrls: ['./buscar-usuario.component.css']
})
export class BuscarUsuarioComponent implements OnInit {

  @Input()
  identificacion: string
  constructor(private service: ServiceUsuarioService,private dataservice:DataAccessServiceService) { }

  ngOnInit(): void {
  }
  buscar(): void{

    this.service.buscarUsuario(this.identificacion).subscribe(
      resultado => {
        this.dataservice.disparador.emit(resultado)
      }
    );
  }

}
