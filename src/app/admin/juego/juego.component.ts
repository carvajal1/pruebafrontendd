import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-juego',
  templateUrl: './juego.component.html',
  styleUrls: ['./juego.component.css']
})
export class JuegoComponent implements OnInit {
  valor1: string;
  valor2: string;
  valor3: string;
  valor4: string;
  valor5: string;
  valor6: string;
  valor7: string;
  valor8: string;
  valor9: string;
  turno: string;
  jugador1: string;
  jugador2: string;
  turnos: number;
  jugadas: string[];
  jugadasGanador: number[][];
  constructor() { }

  ngOnInit(): void {
    this.setValor();
    this.jugadasGanador=[
      [0,1,2],
      [3,4,5],
      [6,7,8],
      [0,3,6],
      [1,4,7],
      [2,5,8],
      [2,4,6],
      [0,4,8]

    ]
  }
  marcar(v: number) :void{

    switch(v){
      case 1:
        this.valor1=this.turno

        break;
      case 2:
        this.valor2=this.turno

        break;
      case 3:
        this.valor3=this.turno

        break;
      case 4:
        this.valor4=this.turno

        break;
       case 5:
        this.valor5=this.turno

        break;
      case 6:
        this.valor6=this.turno

        break;
       case 7:
        this.valor7=this.turno

        break;
      case 8:
        this.valor8=this.turno

        break;
      case 9:
        this.valor9=this.turno

        break;
      default:
        break;
    }
    this.jugadas[v-1]=this.turno;
    this.verificarGanador();
    this.turnos++;
    if(this.turnos==9){
      this.turno="";
      alert("Fin del Juego");
      console.log(this.jugadas)
    }

    this.iniciar();
  }
  verificarGanador() {

    var indices = [];

      var element = this.turno;
      var idx = this.jugadas.indexOf(element);
      while (idx != -1) {
        indices.push(idx);
        idx = this.jugadas.indexOf(element, idx + 1);
      }
      var s=indices.toString();
      console.log(indices);
      if(indices.length>=3){
        for(var i=0;i<this.jugadasGanador.length;i++){
          console.log(this.jugadasGanador[i]);
          console.log(s);
            if(s.search(this.jugadasGanador[i].toString()) != -1 ){
              if(this.turno=="X"){
                alert("Ganador Jugador 2");
              }else{
                alert("Ganador Jugador 1");
              }
              this.setValor();
            }


        }
      }


    console.log(s.length);


  }
  setValor():void{
    this.valor1="☺";
    this.valor2="☺";
    this.valor3="☺";
    this.valor4="☺";
    this.valor5="☺";
    this.valor6="☺";
    this.valor7="☺";
    this.valor8="☺";
    this.valor9="☺";
    this.jugador1="O";
    this.jugador2="X";
    this.turno="";
    this.turnos=0;
    this.jugadas= ["☺", "☺", "☺", "☺", "☺", "☺", "☺", "☺", "☺"];
  }

  iniciar(): void{
    if(this.turno==""){
      this.turno=this.jugador1;
    }else if(this.turno==this.jugador1){
      this.turno=this.jugador2;
    }else if(this.turno==this.jugador2){
      this.turno=this.jugador1;
    }

  }
}
