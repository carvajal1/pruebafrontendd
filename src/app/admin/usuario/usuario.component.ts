import { Usuario } from './../../interfaces/usuario.interfaces';
import { Component, OnInit } from '@angular/core';
import { DataAccessServiceService } from 'src/app/services/data-access-service.service';
import { ServiceUsuarioService } from 'src/app/services/service-usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  nombre: string;
  apellido: string;
  tipoIdentificacion: string;
  tipo: any;
  numeroIdentificacion: string;
  contrasena: string;
  email: string;
  id: string;
  show: false;
  constructor(private service: ServiceUsuarioService,private dataservice:DataAccessServiceService) {

  }

  ngOnInit(): void {
    this.getCategoria();
    this.dataservice.disparador.subscribe((data:Usuario)=>{
      if (data.error != "null"){
        alert(data.detalle);
        this.setDatos(data)
        this.show=false;

      }else{
        console.log(data.detalle+ data.error );

        this.show=false;
      }
      console.log(data)
      console.log(data.detalle)
    })
  }
  setDatos(data: Usuario) {
    this.nombre=data.usuario.nombre;
    this.apellido=data.usuario.apellido;
    this.tipoIdentificacion=data.usuario.tipoIdentificacion;

    this.numeroIdentificacion=data.usuario.numeroIdentificacion;
    this.contrasena="N/a";
    this.email=data.usuario.email;
    this.id=data.usuario.id.toString();
  }
  getCategoria(): void{
    this.service.getTipoDocumento().subscribe(resultado => {
      console.log(resultado);
      this.tipo = resultado.tipo;
    });
  }
  onChange($event): void{
    this.tipoIdentificacion = $event;
    console.log(this.tipoIdentificacion);
  }

  getDato(): FormData{

    const fd = new FormData();
    fd.append('id', "0");
    fd.append('nombre', this.nombre);
    fd.append('apellido', this.apellido);
    fd.append('tipoIdentificacion', this.tipoIdentificacion);
    fd.append('numeroIdentificacion', this.numeroIdentificacion);
    fd.append('contrasena', this.contrasena);
    fd.append('email', this.email);
   return fd;

  }
}
