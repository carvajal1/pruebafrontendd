import { Component, Input, OnInit } from '@angular/core';
import { ServiceUsuarioService } from 'src/app/services/service-usuario.service';

@Component({
  selector: 'app-eliminar-usuario',
  templateUrl: './eliminar-usuario.component.html',
  styleUrls: ['./eliminar-usuario.component.css']
})
export class EliminarUsuarioComponent implements OnInit {
  @Input()
  identificacion: string
  constructor(private service: ServiceUsuarioService) { }

  ngOnInit(): void {
  }
  eliminar(): void{
    this.service.eliminarUsuario(this.identificacion).subscribe(
      resultado => {
        if (resultado.error != "null"){
          alert(resultado.detalle);


        }else{
          console.log(resultado.detalle+ resultado.error );

        }
      }
    );
  }
}
