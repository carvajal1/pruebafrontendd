import { TipoDocumento } from './../../../interfaces/tipoDocumento.interface';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceUsuarioService } from 'src/app/services/service-usuario.service';
import { DataAccessServiceService } from 'src/app/services/data-access-service.service';

@Component({
  selector: 'app-registra-usuario',
  templateUrl: './registra-usuario.component.html',
  styleUrls: ['./registra-usuario.component.css']
})
export class RegistraUsuarioComponent implements OnInit {
  nombre: string;
  apellido: string;
  tipoIdentificacion: string;
  tipo: any;
  numeroIdentificacion: string;
  contrasena: string;
  email: string;

  show = false;
  constructor(private router: Router,private service: ServiceUsuarioService) {

  }

  ngOnInit(): void {
    this.getCategoria();
  }
  getCategoria(): void{
    this.service.getTipoDocumento().subscribe(resultado => {
      console.log(resultado);
      this.tipo = resultado.tipo;
    });
  }
  cancelar(): void{
    this.router.navigateByUrl('');
  }

  onChange($event): void{
    this.tipoIdentificacion = $event;
    console.log(this.tipoIdentificacion);
  }
  registrar(): void{
    const fd = new FormData();
    fd.append('id', "0");
    fd.append('nombre', this.nombre);
    fd.append('apellido', this.apellido);
    fd.append('tipoIdentificacion', this.tipoIdentificacion);
    fd.append('numeroIdentificacion', this.numeroIdentificacion);
    fd.append('contrasena', this.contrasena);
    fd.append('email', this.email);
    this.service.guardarUsuario(fd).subscribe(
      resultado => {
        if (resultado.error != "null"){
          alert(resultado.detalle);

          this.show=false;
          this.router.navigateByUrl('login');
        }else{
          console.log(resultado.detalle+ resultado.error );
          alert(this.service.getError());
          this.show=false;
        }
      }
    );
  }

}
