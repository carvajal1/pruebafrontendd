import { Component, Input, OnInit } from '@angular/core';
import { DataAccessServiceService } from 'src/app/services/data-access-service.service';
import { ServiceUsuarioService } from 'src/app/services/service-usuario.service';

@Component({
  selector: 'app-actualiza-usuario',
  templateUrl: './actualiza-usuario.component.html',
  styleUrls: ['./actualiza-usuario.component.css']
})
export class ActualizaUsuarioComponent implements OnInit {
  @Input()
  usuario:FormData;
  constructor(private dataservice:DataAccessServiceService,private service: ServiceUsuarioService) { }

  ngOnInit(): void {
  }

  actualizar(): void{

    this.service.actualizarUsuario(this.usuario).subscribe(
      resultado => {
        this.dataservice.disparador.emit(resultado)
      }
    );
  }
}
