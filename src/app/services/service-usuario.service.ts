import { Injectable } from '@angular/core';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { Auth } from '../interfaces/auth.interface';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap, timestamp} from 'rxjs/Operators';
import { TipoDocumento } from '../interfaces/tipoDocumento.interface';
import { Usuario } from '../interfaces/usuario.interfaces';

@Injectable({
  providedIn: 'root'
})
export class ServiceUsuarioService {

  private token: string;
  private login = '/api/TokenAuth';
  private consultarTipoDocumento='/api/tiposDocumentos';
  private usuarioApi = '/api/Usuario';
  constructor(private httpClient: HttpClient, private cookies: CookieService) { }

  iniciaSession(datos: FormData): Observable<Auth>{

    return this.httpClient.post<Auth>(this.login, datos)
    .pipe(
      tap(
        (res: Auth) => {
          this.setToken(res.accessToken, res.expireInSeconds.toString(), res.userId.toString(),res.error);
        }
      )
    );
  }

  getTipoDocumento():Observable<TipoDocumento>{
    return this.httpClient.get<TipoDocumento>(this.consultarTipoDocumento)
    .pipe(
      tap(
        (res: TipoDocumento) => {
          return res;
        }
      )
    );
  }

  guardarUsuario(datos: FormData): Observable<Usuario>{
    return this.httpClient.post<Usuario>(this.usuarioApi, datos)
    .pipe(
      tap(
        (res: Usuario) => {
          return res
        }
      )
    );
  }

  actualizarUsuario(datos: FormData): Observable<Usuario>{
    const header = new HttpHeaders()
    .set('Authorization',  `Bearer ${this.getToken()}`);
    return this.httpClient.put<Usuario>(this.usuarioApi, datos, {headers: header })
    .pipe(
      tap(
        (res: Usuario) => {
          return res
        }
      )
    );
  }

  buscarUsuario(documento :string): Observable<Usuario>{
    const header = new HttpHeaders()
    .set('Authorization',  `Bearer ${this.getToken()}`);
    return this.httpClient.get<Usuario>(this.usuarioApi+"/"+documento, {headers: header })
    .pipe(
      tap(
        (res: Usuario) => {
          return res
        }
      )
    );
  }

  eliminarUsuario(documento :string): Observable<Usuario>{
    const header = new HttpHeaders()
    .set('Authorization',  `Bearer ${this.getToken()}`);
    return this.httpClient.delete<Usuario>(this.usuarioApi+"/"+documento, {headers: header })
    .pipe(
      tap(
        (res: Usuario) => {
          return res
        }
      )
    );
  }
  setToken(token: string, expiresIn: string, idToken: string, error: string): void {
    this.cookies.set('token', token);
    this.cookies.set('expiresIn', expiresIn);
    this.cookies.set('idToken', idToken);
    this.cookies.set('error',error);
  }



  getToken(): string {
    return this.cookies.get('token');
  }

  getError(): string {
    return this.cookies.get('error');
  }

}
