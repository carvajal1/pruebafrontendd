import { UsuarioComponent } from './admin/usuario/usuario.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActualizaUsuarioComponent } from './admin/actualizar/actualiza-usuario/actualiza-usuario.component';
import { BuscarUsuarioComponent } from './admin/buscar/buscar-usuario/buscar-usuario.component';
import { EliminarUsuarioComponent } from './admin/eliminar/eliminar-usuario/eliminar-usuario.component';
import { RegistraUsuarioComponent } from './admin/registrar/registra-usuario/registra-usuario.component';
import { LoginComponent } from './login/login/login.component';

const routes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'registra', component: RegistraUsuarioComponent},
    {path: 'actualizar', component: ActualizaUsuarioComponent},
    {path: 'buscar', component: BuscarUsuarioComponent},
    {path: 'eliminar', component: EliminarUsuarioComponent},
    {path: '', component: LoginComponent},
    {path: 'usuario', component: UsuarioComponent}

];

export const app_routing = RouterModule.forRoot(routes);
